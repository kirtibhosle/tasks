import { DDLtrackCampaign, DDLtrackEvent, HotjarTracking} from '../Common/ddl_tracking.js'
import { pollFor } from 'icarus'
import './v1.scss'

const testVar = 'Variation 1';

pollFor('body', initT01)

function initT01() {
    if (document.body.className.indexOf('MAG24_loaded') === -1) {
        DDLtrackCampaign(testVar); // general campaign tracking
        t01Changes();
    } else {
        console.warn('Experiment not loaded');
    }
}


function t01Changes() {
    document.body.classList.add('MAG24_loaded');
    
    document.getElementsByClassName("tray")[1].append((Object.assign(document.createElement('button'),{title:"Style",classList:"swipe-slider-item variant-filter-control newStyle", innerHTML:"Style"})));
    
    document.getElementsByClassName("variant-filter-control")[3].addEventListener('click',function() { 
        document.getElementsByClassName("side-filter-overlay")[0].style.display = "block";
        document.getElementsByClassName("accordion")[3].classList.add("expanded");
        document.getElementsByClassName("accordion-content")[3].attributes[1].value = "height: 80px;";
     });

    document.getElementsByClassName("side-filter__close-button ")[0].addEventListener('click',function() { 
        document.getElementsByClassName("side-filter-overlay")[0].style.display = "none";
    }); 

    var parent=document.getElementsByClassName('side-filter-overlay')[0];
    var child=document.getElementsByClassName('overlay__content')[0];

    parent.addEventListener('click',function(event){
        event.preventDefault();
        if(event.target === parent) {
            document.getElementsByClassName("side-filter-overlay")[0].style.display = "none";  
        }
        if(event.target === child) {
            document.getElementsByClassName("side-filter-overlay")[0].style.display = "block";
        }
    },true);

    var newClose = document.getElementsByClassName('side-filter__actions')[0];
    newClose.addEventListener('click',function() { 
        setTimeout(function(){
            document.getElementsByClassName("side-filter-overlay")[0].style.display = "none";
        }, 1000);
    }); 

    var itm = document.getElementsByClassName("accordion")[2];
    var cln = itm.cloneNode(true);
     cln.getElementsByTagName('span')[0].innerText = "Style";
    document.getElementsByClassName("side-filter__content")[0].append(cln);
    document.getElementsByClassName("accordion-content")[3].innerHTML = '';
   
    var fragment = document.createDocumentFragment();

    fragment.appendChild(document.querySelectorAll("[style='transform: translateX(0px);']")[0]);

    document.getElementsByClassName("accordion-content")[3].appendChild(fragment);

    var newone = document.querySelectorAll(".tray")[2];

    var oldDdl = document.getElementsByClassName('side-filter__check')[0];
    const copyoldDdl = oldDdl.cloneNode(true);
    document.getElementsByClassName('category-filter')[0].prepend(copyoldDdl);
    var oldDdl1 = document.getElementsByClassName('side-filter__check')[0];
    const copyoldDdl1 = oldDdl1.cloneNode(true);
    document.getElementsByClassName('category-filter')[1].prepend(copyoldDdl1);
    var oldDdl2 = document.getElementsByClassName('side-filter__check')[0];
    const copyoldDdl2 = oldDdl2.cloneNode(true);
    document.getElementsByClassName('category-filter')[2].prepend(copyoldDdl2);

    var removepic= newone.getElementsByTagName('picture');
    for(var i=0; i <= removepic.length; i++) {
        removepic[i].remove(removepic[i]);
    }
    removepic[0].remove();
    document.getElementsByClassName("accordion-button")[3].addEventListener('click',function(){
        var element = document.getElementsByClassName("accordion")[3];
        var t = document.getElementsByClassName("accordion-content")[3];
        if(t.attributes[1].value == "height: 0px;"){
            element.classList.add("expanded");
            t.attributes[1].value = "height: 80px;";
        }
        else{
            element.classList.remove("expanded");
            t.attributes[1].value = "height: 0px;"
      }
    });
    
    var NewNodeCategory = document.getElementsByClassName("category-filter");

    for(var i=0; i < NewNodeCategory.length; i++) {
        NewNodeCategory[i].classList.add("NewNodeCategory");
    }
    
    document.getElementsByClassName("category-filter")[0].style.display = "block";
    document.getElementsByClassName("category-filter")[1].style.display = "block";
    document.getElementsByClassName("category-filter")[2].style.display = "block";

    var NewNode = document.getElementsByClassName('category-filter')[0].childNodes[2];
    NewNode.classList.add("NewNode");
    var NewNode1 = document.getElementsByClassName('category-filter')[1].childNodes[2];
    NewNode1.classList.add("NewNode");
    var NewNode2 = document.getElementsByClassName('category-filter')[2].childNodes[2];
    NewNode2.classList.add("NewNode");

  



    // your test changes go here

    // Tracking Helpers
    // DDLtrackEvent(testVar, 'Test CTA clicked', 'with Label', 2); // event tracking (testVar, 'action', 'label', 'value')
    // HotjarTracking(testVar, 'Custom Varible'); // hotjar (testVar, 'Custom Variable')

}